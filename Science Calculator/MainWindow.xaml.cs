﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Input;

namespace Science_Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // init component
        public MainWindow()
        {
            InitializeComponent();
        }

        // !!! Algorithm part

        // Compute example formula and display result to result panel
        /*
        Built in function "Compute" to compute formula from string
        The following arithmetic operators are supported in expressions:
        + (addition)
        - (subtraction)
        * (multiplication)
        / (division)
        % (modulus)
        */
        double computeSimpleFormula(string formula)
        {
            return Convert.ToDouble(new DataTable().Compute(formula, null));
        }

        // exmaple:
        // 123*cos(4*5)+6789 -> (4*5)
        string getSubformula(string formula, string function)
        {
            int length = function.Length;
            return formula.Substring(formula.IndexOf(function) + length-1, formula.IndexOf(')') - (formula.IndexOf(function) + length - 1) + 1);
        }

        string replaceSubResult(string formula, string function, string subresult)
        {
            int length = function.Length;
            int position_of_fnc = formula.IndexOf(function);
            return formula.Remove(position_of_fnc, formula.IndexOf(')') - formula.IndexOf(function) + 1).Insert(position_of_fnc, subresult);
        }

        string makeFormula (string formula)
        {
            // empty formula
            if (formula == "")
            {
                return "";
            }
            else if (formula.Contains("cos("))
            {
                string function = "cos(";
                string subformula = getSubformula(formula, function);
                double subresult = computeSimpleFormula(subformula);

                double use_func = Math.Cos(subresult * Math.PI / 180); // in deg

                use_func = Math.Round(use_func, 12); // round
                formula = replaceSubResult(formula, function, '(' + use_func.ToString() + ')');

            }
            else if (formula.Contains("sin("))
            {
                string function = "sin(";
                string subformula = getSubformula(formula, function);
                double subresult = computeSimpleFormula(subformula);

                double use_func = Math.Sin(subresult * Math.PI / 180); // in deg

                use_func = Math.Round(use_func, 12); // round
                formula = replaceSubResult(formula, function, '(' + use_func.ToString() + ')');
            }
            else if (formula.Contains("tan("))
            {
                string function = "tan(";
                string subformula = getSubformula(formula, function);
                double subresult = computeSimpleFormula(subformula);

                double use_func = Math.Tan(subresult * Math.PI / 180); // in deg

                use_func = Math.Round(use_func, 12); // round
                formula = replaceSubResult(formula, function, '(' + use_func.ToString() + ')');
            }
            else if (formula.Contains("log("))
            {
                string function = "log(";
                string subformula = getSubformula(formula, function);
                double subresult = computeSimpleFormula(subformula);

                double use_func = Math.Log10(subresult);

                use_func = Math.Round(use_func, 12); // round
                formula = replaceSubResult(formula, function, '(' + use_func.ToString() + ')');
            }
            else if (formula.Contains("ln("))
            {
                string function = "ln(";
                string subformula = getSubformula(formula, function);
                double subresult = computeSimpleFormula(subformula);

                double use_func = Math.Log(subresult);

                use_func = Math.Round(use_func, 12); // round
                formula = replaceSubResult(formula, function, '(' + use_func.ToString() + ')');
            }

            return formula;
        }

        private void btn_ok_Click(object sender, RoutedEventArgs e)
        {
            // get formula
            string formula = example.Text;

            formula = makeFormula(formula);

            // compute formula
            double result_double = computeSimpleFormula(formula);
            // display result
            result.Text = result_double.ToString();

            // add to history list: example, result and empty line
            addToHistory("");
            addToHistory(result.Text);
            addToHistory(example.Text);
        }

        // add to history list one line
        void addToHistory(string input)
        {
            history_view.Items.Insert(0, input);
            history_view.Items.Refresh();
            history_column.Width = history_view.ActualWidth - 30; // resize history column
        }

        // check status for reseting result display (if start new formula)
        void checkStatus()
        {
            if (result.Text != "")
            {
                result.Text = "";
                example.Text = "";
            }
        }


        // !!! GUI part

        // hide / unhide history
        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            // hide / unhide history
            if (e.NewSize.Width > 600)
            {
                int history_size = 4;

                history.Visibility = Visibility.Visible;
                top_panel.ColumnDefinitions[8].Width = new GridLength(history_size, GridUnitType.Star);
                main.ColumnDefinitions[8].Width = new GridLength(history_size, GridUnitType.Star);
                func.ColumnDefinitions[6].Width = new GridLength(3, GridUnitType.Star);
                first.ColumnDefinitions[10].Width = new GridLength(5, GridUnitType.Star);
                second.ColumnDefinitions[11].Width = new GridLength(4.95, GridUnitType.Star);
            }
            else
            {
                history.Visibility = Visibility.Hidden;
                top_panel.ColumnDefinitions[8].Width = new GridLength(0, GridUnitType.Star);
                main.ColumnDefinitions[8].Width = new GridLength(0, GridUnitType.Star);
                func.ColumnDefinitions[6].Width = new GridLength(0, GridUnitType.Star);
                first.ColumnDefinitions[10].Width = new GridLength(0, GridUnitType.Star);
                second.ColumnDefinitions[11].Width = new GridLength(0, GridUnitType.Star);
            }

            menu_panel.Height = e.NewSize.Height - 5;
            history_column.Width = history_view.ActualWidth - 30; // resize history column
        }

        // !!! click on select-buttons buttons part

        // main buttons
        private void main_btn_Click(object sender, RoutedEventArgs e)
        {
            func.Visibility = Visibility.Hidden;
            main.Visibility = Visibility.Visible;
            abc.Visibility = Visibility.Hidden;
        }

        // func. buttons
        private void func_btn_Click(object sender, RoutedEventArgs e)
        {
            main.Visibility = Visibility.Hidden;
            abc.Visibility = Visibility.Hidden;
            func.Visibility = Visibility.Visible;
        }

        // abc-keyboards buttons
        private void abc_btn_Click(object sender, RoutedEventArgs e)
        {
            func.Visibility = Visibility.Hidden;
            main.Visibility = Visibility.Hidden;
            abc.Visibility = Visibility.Visible;
        }


        // !!! click on calculator's buttons part

        // Buttons 1-9/*-+.
        private void btn_1_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "1";
            btn_ok.Focus();
        }

        private void btn_2_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "2";
            btn_ok.Focus();
        }

        private void btn_7_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "7";
            btn_ok.Focus();
        }

        private void btn_4_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "4";
            btn_ok.Focus();
        }

        private void btn_0_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "0";
            btn_ok.Focus();
        }

        private void btn_8_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "8";
            btn_ok.Focus();
        }

        private void btn_5_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "5";
            btn_ok.Focus();
        }

        private void btn_dot_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += ".";
            btn_ok.Focus();
        }

        private void btn_9_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "9";
            btn_ok.Focus();
        }

        private void btn_6_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "6";
            btn_ok.Focus();
        }

        private void btn_3_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "3";
            btn_ok.Focus();
        }

        private void btn_ans_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn_div_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "/";
            btn_ok.Focus();
        }

        private void btn_mul_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "*";
            btn_ok.Focus();
        }

        private void btn_minus_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "-";
            btn_ok.Focus();
        }

        private void btn_plus_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "+";
            btn_ok.Focus();
        }
        
        // other buttons
        private void btn_sin_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "sin(";
            btn_ok.Focus();
        }

        private void btn_log_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "log(";
            btn_ok.Focus();
        }

        private void btn_cos_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "cos(";
            btn_ok.Focus();
        }

        private void btn_ln_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "ln(";
            btn_ok.Focus();
        }

        private void btn_tan_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "tan(";
            btn_ok.Focus();
        }

        private void btn_left_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "(";
            btn_ok.Focus();
        }

        private void btn_right_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += ")";
            btn_ok.Focus();
        }

        private void btn_cos2_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "cos(";
            btn_ok.Focus();
        }

        private void btn_tan2_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "tan(";
            btn_ok.Focus();
        }

        private void btn_sin2_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "sin(";
            btn_ok.Focus();
        }

        private void btn_log2_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "log(";
            btn_ok.Focus();
        }

        private void btn_ln2_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "ln(";
            btn_ok.Focus();
        }

        private void btn_t_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "t";
        }

        private void btn_i_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "i";
        }

        private void btn_o_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "o";
        }

        private void btn_c_Click_1(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "c";
        }

        private void btn_n_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "n";
        }

        private void btn_dot2_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += ".";
        }

        private void btn_a_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "a";
        }

        private void btn_s_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "s";
        }

        private void btn_g_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "g";
        }

        private void btn_l_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "l";
        }

        private void btn_ok2_Click(object sender, RoutedEventArgs e)
        {
            btn_ok_Click(this, null);
        }

        private void btn_left2_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += "(";
        }

        private void btn_right2_Click(object sender, RoutedEventArgs e)
        {
            checkStatus();
            example.Text += ")";
        }

        // delete last char from example formula
        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            if (result.Text == "" && example.Text != "")
            {
                string exam = example.Text;
                example.Text = exam.Remove(exam.Length - 1);
            }
            btn_ok.Focus();
        }

        // delete all from example even from result
        private void btn_C_Click(object sender, RoutedEventArgs e)
        {
            result.Text = "";
            example.Text = "";
            btn_ok.Focus();
        }

        // clear all history and example and result
        private void clear_all_btn_Click(object sender, RoutedEventArgs e)
        {
            result.Text = "";
            example.Text = "";
            history_view.Items.Clear();
            btn_ok.Focus();
        }

        // !!! keyboard part

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.NumPad0 || e.Key == Key.D0)
            {
                checkStatus();
                example.Text += "0";
            }
            else if (e.Key == Key.NumPad1 || e.Key == Key.D1)
            {
                checkStatus();
                example.Text += "1";
            }
            else if (e.Key == Key.NumPad2 || e.Key == Key.D2)
            {
                checkStatus();
                example.Text += "2";
            }
            else if (e.Key == Key.NumPad3 || e.Key == Key.D3)
            {
                checkStatus();
                example.Text += "3";
            }
            else if (e.Key == Key.NumPad4 || e.Key == Key.D4)
            {
                checkStatus();
                example.Text += "4";
            }
            else if (e.Key == Key.NumPad5 || e.Key == Key.D5)
            {
                checkStatus();
                example.Text += "5";
            }
            else if (e.Key == Key.NumPad6 || e.Key == Key.D6)
            {
                checkStatus();
                example.Text += "6";
            }
            else if (e.Key == Key.NumPad7 || e.Key == Key.D7)
            {
                checkStatus();
                example.Text += "7";
            }
            else if (e.Key == Key.NumPad8 || e.Key == Key.D8)
            {
                checkStatus();
                example.Text += "8";
            }
            else if (e.Key == Key.NumPad9 || e.Key == Key.D9)
            {
                checkStatus();
                example.Text += "9";
            }
            else if (e.Key == Key.Add || e.Key == Key.OemPlus)
            {
                checkStatus();
                example.Text += "+";
            }
            else if (e.Key == Key.Divide)
            {
                checkStatus();
                example.Text += "/";
            }
            else if (e.Key == Key.Multiply)
            {
                checkStatus();
                example.Text += "*";
            }
            else if (e.Key == Key.Subtract || e.Key == Key.OemMinus)
            {
                checkStatus();
                example.Text += "-";
            }
            else if (e.Key == Key.OemPeriod || e.Key == Key.Decimal)
            {
                checkStatus();
                example.Text += ".";
            }
            // perform click =
            else if (e.Key == Key.Return)
            {
                btn_ok_Click(this, null);
            }
            // perform click back
            else if (e.Key == Key.Back)
            {
                btn_back_Click(this, null);
            }
        }

        // !!! hot key
        // TODO
        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            // Alt + C -> cos( 
            if (Keyboard.IsKeyDown(Key.LeftAlt) && Keyboard.IsKeyDown(Key.C))
            {
                checkStatus();
                example.Text += "cos(";
                e.Handled = true; // disable "Ding" tone
            }
            else if (Keyboard.IsKeyDown(Key.LeftAlt) && Keyboard.IsKeyDown(Key.S))
            {
                checkStatus();
                example.Text += "sin(";
                e.Handled = true; // disable "Ding" tone
            }
            else if (Keyboard.IsKeyDown(Key.LeftAlt) && Keyboard.IsKeyDown(Key.T))
            {
                checkStatus();
                example.Text += "tan(";
                e.Handled = true; // disable "Ding" tone
            }
            else if (Keyboard.IsKeyDown(Key.LeftAlt) && Keyboard.IsKeyDown(Key.L))
            {
                checkStatus();
                example.Text += "log(";
                e.Handled = true; // disable "Ding" tone
            }
            else if (Keyboard.IsKeyDown(Key.LeftAlt) && Keyboard.IsKeyDown(Key.N))
            {
                checkStatus();
                example.Text += "ln(";
                e.Handled = true; // disable "Ding" tone
            }
            else if (Keyboard.IsKeyDown(Key.LeftAlt) && Keyboard.IsKeyDown(Key.P))
            {
                checkStatus();
                example.Text = "";
                e.Handled = true; // disable "Ding" tone
            }

            else if (Keyboard.IsKeyDown(Key.LeftShift) && Keyboard.IsKeyDown(Key.D9))
            {
                checkStatus();
                example.Text += "(";
                e.Handled = true;
            }
            else if (Keyboard.IsKeyDown(Key.LeftShift) && Keyboard.IsKeyDown(Key.D0))
            {
                checkStatus();
                example.Text += ")";
                e.Handled = true;
            }
        }

        // !!! menu
        private void menu_btn_Click(object sender, RoutedEventArgs e)
        {
            menu.Visibility = Visibility.Visible;
        }

        private void menu_panel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            menu.Visibility = Visibility.Hidden;

        }

        private void menu_panel_MouseLeave(object sender, MouseEventArgs e)
        {
            //menu.Visibility = Visibility.Hidden;
        }

        private void menu_label_MouseDown(object sender, MouseButtonEventArgs e)
        {
            menu.Visibility = Visibility.Hidden;
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            menu.Visibility = Visibility.Hidden;
        }

        // menu click
        private void hot_btn_cos_MouseDown(object sender, MouseButtonEventArgs e)
        {
            checkStatus();
            example.Text += "cos(";
            menu.Visibility = Visibility.Hidden;
            btn_ok.Focus();
        }

        // menu click
        private void hot_btn_sin_MouseDown(object sender, MouseButtonEventArgs e)
        {
            checkStatus();
            example.Text += "sin(";
            menu.Visibility = Visibility.Hidden;
            btn_ok.Focus();
        }

        // menu click
        private void hot_btn_tan_MouseDown(object sender, MouseButtonEventArgs e)
        {
            checkStatus();
            example.Text += "tan(";
            menu.Visibility = Visibility.Hidden;
            btn_ok.Focus();
        }

        // menu click
        private void hot_btn_log_MouseDown(object sender, MouseButtonEventArgs e)
        {
            checkStatus();
            example.Text += "log(";
            menu.Visibility = Visibility.Hidden;
            btn_ok.Focus();
        }

        // menu click
        private void hot_btn_ln_MouseDown(object sender, MouseButtonEventArgs e)
        {
            checkStatus();
            example.Text += "ln(";
            menu.Visibility = Visibility.Hidden;
            btn_ok.Focus();
        }

        private void result_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Clipboard.SetText(result.Text);
        }
    }
}