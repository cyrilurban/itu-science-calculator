#pragma checksum "..\..\MainWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4DCF7ACE907560A08FE57310D5D851FD"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Science_Calculator;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Science_Calculator
{


    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector
    {

#line default
#line hidden


#line 104 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid history;

#line default
#line hidden


#line 131 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView history_view;

#line default
#line hidden


#line 139 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GridViewColumn history_column;

#line default
#line hidden


#line 147 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid main;

#line default
#line hidden


#line 171 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border input_background;

#line default
#line hidden


#line 172 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_7;

#line default
#line hidden


#line 173 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_4;

#line default
#line hidden


#line 174 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_1;

#line default
#line hidden


#line 175 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_0;

#line default
#line hidden


#line 176 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_8;

#line default
#line hidden


#line 177 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_5;

#line default
#line hidden


#line 178 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_2;

#line default
#line hidden


#line 179 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_dot;

#line default
#line hidden


#line 180 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_9;

#line default
#line hidden


#line 181 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_6;

#line default
#line hidden


#line 182 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_3;

#line default
#line hidden


#line 183 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_ans;

#line default
#line hidden


#line 184 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_div;

#line default
#line hidden


#line 185 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_mul;

#line default
#line hidden


#line 186 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_minus;

#line default
#line hidden


#line 187 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_plus;

#line default
#line hidden


#line 190 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_sin;

#line default
#line hidden


#line 191 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_log;

#line default
#line hidden


#line 194 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_cos;

#line default
#line hidden


#line 195 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_ln;

#line default
#line hidden


#line 197 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_left;

#line default
#line hidden


#line 198 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_tan;

#line default
#line hidden


#line 201 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_right;

#line default
#line hidden


#line 203 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_ok;

#line default
#line hidden


#line 206 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid func;

#line default
#line hidden


#line 228 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_cos2;

#line default
#line hidden


#line 229 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_tan2;

#line default
#line hidden


#line 230 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_sin2;

#line default
#line hidden


#line 231 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_log2;

#line default
#line hidden


#line 235 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_ln2;

#line default
#line hidden


#line 254 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid abc;

#line default
#line hidden


#line 255 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid first;

#line default
#line hidden


#line 286 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_t;

#line default
#line hidden


#line 289 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_i;

#line default
#line hidden


#line 290 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_o;

#line default
#line hidden


#line 296 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_c;

#line default
#line hidden


#line 299 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_n;

#line default
#line hidden


#line 302 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_dot2;

#line default
#line hidden


#line 304 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid second;

#line default
#line hidden


#line 331 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_a;

#line default
#line hidden


#line 332 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_s;

#line default
#line hidden


#line 335 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_g;

#line default
#line hidden


#line 339 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_l;

#line default
#line hidden


#line 341 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_ok2;

#line default
#line hidden


#line 342 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_left2;

#line default
#line hidden


#line 343 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_right2;

#line default
#line hidden


#line 354 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid top_panel;

#line default
#line hidden


#line 379 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button menu_btn;

#line default
#line hidden


#line 382 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock example;

#line default
#line hidden


#line 385 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock result;

#line default
#line hidden


#line 388 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button main_btn;

#line default
#line hidden


#line 389 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button func_btn;

#line default
#line hidden


#line 390 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button abc_btn;

#line default
#line hidden


#line 391 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_back;

#line default
#line hidden


#line 392 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_C;

#line default
#line hidden


#line 393 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button clear_all_btn;

#line default
#line hidden


#line 396 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid menu;

#line default
#line hidden


#line 420 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border menu_panel;

#line default
#line hidden


#line 421 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label menu_label;

#line default
#line hidden


#line 422 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label hot_btn_cos;

#line default
#line hidden


#line 423 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label hot_btn_sin;

#line default
#line hidden


#line 424 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label hot_btn_tan;

#line default
#line hidden


#line 425 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label hot_btn_log;

#line default
#line hidden


#line 426 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label hot_btn_ln;

#line default
#line hidden

        private bool _contentLoaded;

        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent()
        {
            if (_contentLoaded)
            {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Science Calculator;component/mainwindow.xaml", System.UriKind.Relative);

#line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);

#line default
#line hidden
        }

        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
        {
            switch (connectionId)
            {
                case 1:
                    this.mainWindow = ((Science_Calculator.MainWindow)(target));

#line 8 "..\..\MainWindow.xaml"
                    this.mainWindow.SizeChanged += new System.Windows.SizeChangedEventHandler(this.Window_SizeChanged);

#line default
#line hidden

#line 8 "..\..\MainWindow.xaml"
                    this.mainWindow.KeyDown += new System.Windows.Input.KeyEventHandler(this.Window_KeyDown);

#line default
#line hidden

#line 8 "..\..\MainWindow.xaml"
                    this.mainWindow.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.Window_PreviewKeyDown);

#line default
#line hidden
                    return;
                case 2:
                    this.history = ((System.Windows.Controls.Grid)(target));
                    return;
                case 3:
                    this.history_view = ((System.Windows.Controls.ListView)(target));
                    return;
                case 4:
                    this.history_column = ((System.Windows.Controls.GridViewColumn)(target));
                    return;
                case 5:
                    this.main = ((System.Windows.Controls.Grid)(target));
                    return;
                case 6:
                    this.input_background = ((System.Windows.Controls.Border)(target));
                    return;
                case 7:
                    this.btn_7 = ((System.Windows.Controls.Button)(target));

#line 172 "..\..\MainWindow.xaml"
                    this.btn_7.Click += new System.Windows.RoutedEventHandler(this.btn_7_Click);

#line default
#line hidden
                    return;
                case 8:
                    this.btn_4 = ((System.Windows.Controls.Button)(target));

#line 173 "..\..\MainWindow.xaml"
                    this.btn_4.Click += new System.Windows.RoutedEventHandler(this.btn_4_Click);

#line default
#line hidden
                    return;
                case 9:
                    this.btn_1 = ((System.Windows.Controls.Button)(target));

#line 174 "..\..\MainWindow.xaml"
                    this.btn_1.Click += new System.Windows.RoutedEventHandler(this.btn_1_Click);

#line default
#line hidden
                    return;
                case 10:
                    this.btn_0 = ((System.Windows.Controls.Button)(target));

#line 175 "..\..\MainWindow.xaml"
                    this.btn_0.Click += new System.Windows.RoutedEventHandler(this.btn_0_Click);

#line default
#line hidden
                    return;
                case 11:
                    this.btn_8 = ((System.Windows.Controls.Button)(target));

#line 176 "..\..\MainWindow.xaml"
                    this.btn_8.Click += new System.Windows.RoutedEventHandler(this.btn_8_Click);

#line default
#line hidden
                    return;
                case 12:
                    this.btn_5 = ((System.Windows.Controls.Button)(target));

#line 177 "..\..\MainWindow.xaml"
                    this.btn_5.Click += new System.Windows.RoutedEventHandler(this.btn_5_Click);

#line default
#line hidden
                    return;
                case 13:
                    this.btn_2 = ((System.Windows.Controls.Button)(target));

#line 178 "..\..\MainWindow.xaml"
                    this.btn_2.Click += new System.Windows.RoutedEventHandler(this.btn_2_Click);

#line default
#line hidden
                    return;
                case 14:
                    this.btn_dot = ((System.Windows.Controls.Button)(target));

#line 179 "..\..\MainWindow.xaml"
                    this.btn_dot.Click += new System.Windows.RoutedEventHandler(this.btn_dot_Click);

#line default
#line hidden
                    return;
                case 15:
                    this.btn_9 = ((System.Windows.Controls.Button)(target));

#line 180 "..\..\MainWindow.xaml"
                    this.btn_9.Click += new System.Windows.RoutedEventHandler(this.btn_9_Click);

#line default
#line hidden
                    return;
                case 16:
                    this.btn_6 = ((System.Windows.Controls.Button)(target));

#line 181 "..\..\MainWindow.xaml"
                    this.btn_6.Click += new System.Windows.RoutedEventHandler(this.btn_6_Click);

#line default
#line hidden
                    return;
                case 17:
                    this.btn_3 = ((System.Windows.Controls.Button)(target));

#line 182 "..\..\MainWindow.xaml"
                    this.btn_3.Click += new System.Windows.RoutedEventHandler(this.btn_3_Click);

#line default
#line hidden
                    return;
                case 18:
                    this.btn_ans = ((System.Windows.Controls.Button)(target));

#line 183 "..\..\MainWindow.xaml"
                    this.btn_ans.Click += new System.Windows.RoutedEventHandler(this.btn_ans_Click);

#line default
#line hidden
                    return;
                case 19:
                    this.btn_div = ((System.Windows.Controls.Button)(target));

#line 184 "..\..\MainWindow.xaml"
                    this.btn_div.Click += new System.Windows.RoutedEventHandler(this.btn_div_Click);

#line default
#line hidden
                    return;
                case 20:
                    this.btn_mul = ((System.Windows.Controls.Button)(target));

#line 185 "..\..\MainWindow.xaml"
                    this.btn_mul.Click += new System.Windows.RoutedEventHandler(this.btn_mul_Click);

#line default
#line hidden
                    return;
                case 21:
                    this.btn_minus = ((System.Windows.Controls.Button)(target));

#line 186 "..\..\MainWindow.xaml"
                    this.btn_minus.Click += new System.Windows.RoutedEventHandler(this.btn_minus_Click);

#line default
#line hidden
                    return;
                case 22:
                    this.btn_plus = ((System.Windows.Controls.Button)(target));

#line 187 "..\..\MainWindow.xaml"
                    this.btn_plus.Click += new System.Windows.RoutedEventHandler(this.btn_plus_Click);

#line default
#line hidden
                    return;
                case 23:
                    this.btn_sin = ((System.Windows.Controls.Button)(target));

#line 190 "..\..\MainWindow.xaml"
                    this.btn_sin.Click += new System.Windows.RoutedEventHandler(this.btn_sin_Click);

#line default
#line hidden
                    return;
                case 24:
                    this.btn_log = ((System.Windows.Controls.Button)(target));

#line 191 "..\..\MainWindow.xaml"
                    this.btn_log.Click += new System.Windows.RoutedEventHandler(this.btn_log_Click);

#line default
#line hidden
                    return;
                case 25:
                    this.btn_cos = ((System.Windows.Controls.Button)(target));

#line 194 "..\..\MainWindow.xaml"
                    this.btn_cos.Click += new System.Windows.RoutedEventHandler(this.btn_cos_Click);

#line default
#line hidden
                    return;
                case 26:
                    this.btn_ln = ((System.Windows.Controls.Button)(target));

#line 195 "..\..\MainWindow.xaml"
                    this.btn_ln.Click += new System.Windows.RoutedEventHandler(this.btn_ln_Click);

#line default
#line hidden
                    return;
                case 27:
                    this.btn_left = ((System.Windows.Controls.Button)(target));

#line 197 "..\..\MainWindow.xaml"
                    this.btn_left.Click += new System.Windows.RoutedEventHandler(this.btn_left_Click);

#line default
#line hidden
                    return;
                case 28:
                    this.btn_tan = ((System.Windows.Controls.Button)(target));

#line 198 "..\..\MainWindow.xaml"
                    this.btn_tan.Click += new System.Windows.RoutedEventHandler(this.btn_tan_Click);

#line default
#line hidden
                    return;
                case 29:
                    this.btn_right = ((System.Windows.Controls.Button)(target));

#line 201 "..\..\MainWindow.xaml"
                    this.btn_right.Click += new System.Windows.RoutedEventHandler(this.btn_right_Click);

#line default
#line hidden
                    return;
                case 30:
                    this.btn_ok = ((System.Windows.Controls.Button)(target));

#line 203 "..\..\MainWindow.xaml"
                    this.btn_ok.Click += new System.Windows.RoutedEventHandler(this.btn_ok_Click);

#line default
#line hidden
                    return;
                case 31:
                    this.func = ((System.Windows.Controls.Grid)(target));
                    return;
                case 32:
                    this.btn_cos2 = ((System.Windows.Controls.Button)(target));

#line 228 "..\..\MainWindow.xaml"
                    this.btn_cos2.Click += new System.Windows.RoutedEventHandler(this.btn_cos2_Click);

#line default
#line hidden
                    return;
                case 33:
                    this.btn_tan2 = ((System.Windows.Controls.Button)(target));

#line 229 "..\..\MainWindow.xaml"
                    this.btn_tan2.Click += new System.Windows.RoutedEventHandler(this.btn_tan2_Click);

#line default
#line hidden
                    return;
                case 34:
                    this.btn_sin2 = ((System.Windows.Controls.Button)(target));

#line 230 "..\..\MainWindow.xaml"
                    this.btn_sin2.Click += new System.Windows.RoutedEventHandler(this.btn_sin2_Click);

#line default
#line hidden
                    return;
                case 35:
                    this.btn_log2 = ((System.Windows.Controls.Button)(target));

#line 231 "..\..\MainWindow.xaml"
                    this.btn_log2.Click += new System.Windows.RoutedEventHandler(this.btn_log2_Click);

#line default
#line hidden
                    return;
                case 36:
                    this.btn_ln2 = ((System.Windows.Controls.Button)(target));

#line 235 "..\..\MainWindow.xaml"
                    this.btn_ln2.Click += new System.Windows.RoutedEventHandler(this.btn_ln2_Click);

#line default
#line hidden
                    return;
                case 37:
                    this.abc = ((System.Windows.Controls.Grid)(target));
                    return;
                case 38:
                    this.first = ((System.Windows.Controls.Grid)(target));
                    return;
                case 39:
                    this.btn_t = ((System.Windows.Controls.Button)(target));

#line 286 "..\..\MainWindow.xaml"
                    this.btn_t.Click += new System.Windows.RoutedEventHandler(this.btn_t_Click);

#line default
#line hidden
                    return;
                case 40:
                    this.btn_i = ((System.Windows.Controls.Button)(target));

#line 289 "..\..\MainWindow.xaml"
                    this.btn_i.Click += new System.Windows.RoutedEventHandler(this.btn_i_Click);

#line default
#line hidden
                    return;
                case 41:
                    this.btn_o = ((System.Windows.Controls.Button)(target));

#line 290 "..\..\MainWindow.xaml"
                    this.btn_o.Click += new System.Windows.RoutedEventHandler(this.btn_o_Click);

#line default
#line hidden
                    return;
                case 42:
                    this.btn_c = ((System.Windows.Controls.Button)(target));

#line 296 "..\..\MainWindow.xaml"
                    this.btn_c.Click += new System.Windows.RoutedEventHandler(this.btn_c_Click_1);

#line default
#line hidden
                    return;
                case 43:
                    this.btn_n = ((System.Windows.Controls.Button)(target));

#line 299 "..\..\MainWindow.xaml"
                    this.btn_n.Click += new System.Windows.RoutedEventHandler(this.btn_n_Click);

#line default
#line hidden
                    return;
                case 44:
                    this.btn_dot2 = ((System.Windows.Controls.Button)(target));

#line 302 "..\..\MainWindow.xaml"
                    this.btn_dot2.Click += new System.Windows.RoutedEventHandler(this.btn_dot2_Click);

#line default
#line hidden
                    return;
                case 45:
                    this.second = ((System.Windows.Controls.Grid)(target));
                    return;
                case 46:
                    this.btn_a = ((System.Windows.Controls.Button)(target));

#line 331 "..\..\MainWindow.xaml"
                    this.btn_a.Click += new System.Windows.RoutedEventHandler(this.btn_a_Click);

#line default
#line hidden
                    return;
                case 47:
                    this.btn_s = ((System.Windows.Controls.Button)(target));

#line 332 "..\..\MainWindow.xaml"
                    this.btn_s.Click += new System.Windows.RoutedEventHandler(this.btn_s_Click);

#line default
#line hidden
                    return;
                case 48:
                    this.btn_g = ((System.Windows.Controls.Button)(target));

#line 335 "..\..\MainWindow.xaml"
                    this.btn_g.Click += new System.Windows.RoutedEventHandler(this.btn_g_Click);

#line default
#line hidden
                    return;
                case 49:
                    this.btn_l = ((System.Windows.Controls.Button)(target));

#line 339 "..\..\MainWindow.xaml"
                    this.btn_l.Click += new System.Windows.RoutedEventHandler(this.btn_l_Click);

#line default
#line hidden
                    return;
                case 50:
                    this.btn_ok2 = ((System.Windows.Controls.Button)(target));

#line 341 "..\..\MainWindow.xaml"
                    this.btn_ok2.Click += new System.Windows.RoutedEventHandler(this.btn_ok2_Click);

#line default
#line hidden
                    return;
                case 51:
                    this.btn_left2 = ((System.Windows.Controls.Button)(target));

#line 342 "..\..\MainWindow.xaml"
                    this.btn_left2.Click += new System.Windows.RoutedEventHandler(this.btn_left2_Click);

#line default
#line hidden
                    return;
                case 52:
                    this.btn_right2 = ((System.Windows.Controls.Button)(target));

#line 343 "..\..\MainWindow.xaml"
                    this.btn_right2.Click += new System.Windows.RoutedEventHandler(this.btn_right2_Click);

#line default
#line hidden
                    return;
                case 53:
                    this.top_panel = ((System.Windows.Controls.Grid)(target));
                    return;
                case 54:
                    this.menu_btn = ((System.Windows.Controls.Button)(target));

#line 379 "..\..\MainWindow.xaml"
                    this.menu_btn.Click += new System.Windows.RoutedEventHandler(this.menu_btn_Click);

#line default
#line hidden
                    return;
                case 55:
                    this.example = ((System.Windows.Controls.TextBlock)(target));
                    return;
                case 56:
                    this.result = ((System.Windows.Controls.TextBlock)(target));

#line 385 "..\..\MainWindow.xaml"
                    this.result.MouseRightButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.result_MouseRightButtonDown);

#line default
#line hidden
                    return;
                case 57:
                    this.main_btn = ((System.Windows.Controls.Button)(target));

#line 388 "..\..\MainWindow.xaml"
                    this.main_btn.Click += new System.Windows.RoutedEventHandler(this.main_btn_Click);

#line default
#line hidden
                    return;
                case 58:
                    this.func_btn = ((System.Windows.Controls.Button)(target));

#line 389 "..\..\MainWindow.xaml"
                    this.func_btn.Click += new System.Windows.RoutedEventHandler(this.func_btn_Click);

#line default
#line hidden
                    return;
                case 59:
                    this.abc_btn = ((System.Windows.Controls.Button)(target));

#line 390 "..\..\MainWindow.xaml"
                    this.abc_btn.Click += new System.Windows.RoutedEventHandler(this.abc_btn_Click);

#line default
#line hidden
                    return;
                case 60:
                    this.btn_back = ((System.Windows.Controls.Button)(target));

#line 391 "..\..\MainWindow.xaml"
                    this.btn_back.Click += new System.Windows.RoutedEventHandler(this.btn_back_Click);

#line default
#line hidden
                    return;
                case 61:
                    this.btn_C = ((System.Windows.Controls.Button)(target));

#line 392 "..\..\MainWindow.xaml"
                    this.btn_C.Click += new System.Windows.RoutedEventHandler(this.btn_C_Click);

#line default
#line hidden
                    return;
                case 62:
                    this.clear_all_btn = ((System.Windows.Controls.Button)(target));

#line 393 "..\..\MainWindow.xaml"
                    this.clear_all_btn.Click += new System.Windows.RoutedEventHandler(this.clear_all_btn_Click);

#line default
#line hidden
                    return;
                case 63:
                    this.menu = ((System.Windows.Controls.Grid)(target));
                    return;
                case 64:
                    this.menu_panel = ((System.Windows.Controls.Border)(target));

#line 420 "..\..\MainWindow.xaml"
                    this.menu_panel.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.menu_panel_MouseDown);

#line default
#line hidden

#line 420 "..\..\MainWindow.xaml"
                    this.menu_panel.MouseLeave += new System.Windows.Input.MouseEventHandler(this.menu_panel_MouseLeave);

#line default
#line hidden
                    return;
                case 65:
                    this.menu_label = ((System.Windows.Controls.Label)(target));

#line 421 "..\..\MainWindow.xaml"
                    this.menu_label.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.menu_label_MouseDown);

#line default
#line hidden
                    return;
                case 66:
                    this.hot_btn_cos = ((System.Windows.Controls.Label)(target));

#line 422 "..\..\MainWindow.xaml"
                    this.hot_btn_cos.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.hot_btn_cos_MouseDown);

#line default
#line hidden
                    return;
                case 67:
                    this.hot_btn_sin = ((System.Windows.Controls.Label)(target));

#line 423 "..\..\MainWindow.xaml"
                    this.hot_btn_sin.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.hot_btn_sin_MouseDown);

#line default
#line hidden
                    return;
                case 68:
                    this.hot_btn_tan = ((System.Windows.Controls.Label)(target));

#line 424 "..\..\MainWindow.xaml"
                    this.hot_btn_tan.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.hot_btn_tan_MouseDown);

#line default
#line hidden
                    return;
                case 69:
                    this.hot_btn_log = ((System.Windows.Controls.Label)(target));

#line 425 "..\..\MainWindow.xaml"
                    this.hot_btn_log.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.hot_btn_log_MouseDown);

#line default
#line hidden
                    return;
                case 70:
                    this.hot_btn_ln = ((System.Windows.Controls.Label)(target));

#line 426 "..\..\MainWindow.xaml"
                    this.hot_btn_ln.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.hot_btn_ln_MouseDown);

#line default
#line hidden
                    return;
            }
            this._contentLoaded = true;
        }

        internal System.Windows.Window mainWindow;
    }
}

